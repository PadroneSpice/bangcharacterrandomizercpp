/*
* Character header for BANG! Character Randomizer
* by Sean Castillo
* an updated remake of my Java BANG! CR
* Version 1.0
* 30 December 2015
*/
#ifndef CHARACTER_H
#define CHARACTER_H
#include <iostream>

using namespace std;

class Character
{
    public:
        Character();
        void setName(string n);
        string getName();
        void setNumber(int num);
        int getNumber();
        void printCharacter();
        virtual ~Character();
    private:
        string name;
        int number;
};

#endif // CHARACTER_H
