/*
* Character source file for BANG! Character Randomizer
* by Sean Castillo
* an updated remake of my Java BANG! CR
* Version 1.0
* 30 December 2015
*/
#include "Character.h"
#include <iostream>
#include <fstream>

using namespace std;

Character::Character()
{
    name = "Unknown";
    number = 0;
}

void Character::setName(string n)
{
    name = n;
}

string Character::getName()
{
    return name;
}

void Character::setNumber(int num)
{
    number = num;
}

int Character::getNumber()
{
    return number;
}

void Character::printCharacter()
{
    cout << number << ": " << name << endl;
}

Character::~Character()
{
    //dtor
}
