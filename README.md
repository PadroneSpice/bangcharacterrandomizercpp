## BANG! Character Randomizer: C++ Edition (made after Java Edition)

by Sean Castillo

version 1.1

30 December 2015

## Character Input Instructions

-The BANG! characters must be written in Characters.txt, one per line.
-The characters should be written in the correct order (at least by volume)
 so that the full roster print may be accurate.


## Running Instructions

1.Click 'CppBangCR.exe'

2.The rest is explained by the program.