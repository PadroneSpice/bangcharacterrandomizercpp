/*
* Main source file for BANG! Character Randomizer
* by Sean Castillo
* an updated remake of my Java BANG! CR
* Version 1.1
* 30 December 2015
*/
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <algorithm>
#include "Character.h"

using namespace std;

//for the characters
string characterName;
int characterNum;
int charVolCount; //means 'character volume counter'. It counts the number of the character in its own volume,
                  //so that the volumes could be given individual text headers.
int volumeCounter; //incremented by the characters; 8 characters = 1 volume

//for the players
int playerCount;
int choiceCount;

//for the input check
bool inputIsValid = false;


//input checking method
bool inputCheck(int playerCount, int choiceCount, int characterCount)
{
    bool valid = false;
    if ((playerCount < 1) || (playerCount > 8))
    {
        cout << "Error 1: Invalid number of players." << endl;
    }
    else if (choiceCount < 1)
    {
        cout << "Error 2: Invalid number of character choices per player." << endl;
    }
    else if (characterCount < 1)
    {
        cout << "Error 3: Characters.txt is empty." << endl;
    }
    else if (playerCount > characterCount)
    {
        cout << "Error 4: Not enough characters for that number of players." << endl;
    }
    else if ((characterCount / choiceCount) < playerCount)
    {
        cout << "Error 5: Not enough characters for that number of choices and players." << endl;
    }
    else
    {
        valid = true;
    }
    return valid;
}

//main method
int main()
{
    //declaring the roster vector
    vector<Character> roster;

    //taking input from the given text file
    string characterLine;
    ifstream characterFile ("Characters.txt");

    //prints the entire roster while populating the roster
    if (characterFile.is_open())
    {
        cout << "____Character Roster____" << endl;
        cout << endl;
        volumeCounter = 1; //starts with volume 1
        while (getline (characterFile, characterLine))
        {
            //the print statement will only go off at the beginning of each volume list.
            if (charVolCount == 0)
            {
                cout << "___" << "Volume " << volumeCounter << "___" << endl;
            }
            characterNum++;
            charVolCount++;

            //prints the character info
            cout << characterNum << ": " << characterLine << '\n'; //I'm guessing this is a little faster than using the get methods from Character to print the info.
            //creates the Character object
            Character c;
            c.setName(characterLine);
            c.setNumber(characterNum);
            //adds the Character to the roster
            roster.push_back(c);

            //separates the characters by volume
            if ((characterNum % 8) == 0)
            {
                cout << endl;
                volumeCounter++;
                charVolCount = 0;
            }
        }
        cout << "________________________" << endl;
        cout << endl;
        characterFile.close();

        //shuffling the roster
        std::srand(unsigned (std::time(0)));
        std::random_shuffle(roster.begin(), roster.end());
    }

    //input prompt and gathering
    cout << "Enter the number of players: " << endl;
       cin >> playerCount;
    cout << "Enter the number of characters from which each player may choose: " << endl;
       cin >> choiceCount;

    cout << endl;

    //checks input for validity
    inputIsValid = inputCheck(playerCount, choiceCount, roster.size()); //updates the inputIsValid bool based on a check given
                                                                        //playerCount, choiceCount, and number of characters in the roster
    //if the input is valid, the characters are distributed.
    if (inputIsValid == true)
    {
        //distributing the characters to the players
        characterNum = 0;
        for (int i = 1; i < playerCount + 1; i++)
        {
            cout << "__Player " << i << "__" << endl;

            for (int j = 0; j < choiceCount; j++)
            {
                roster[characterNum].printCharacter();
                characterNum++;
            }
            cout << endl;
        }
    }
    else
    {
        cout << "Follow the instructions next time, you novice." << endl;
    }
    system("pause"); //prevents the program window from automatically closing when used outside an IDE
    return 0;
}
